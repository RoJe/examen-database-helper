﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;

namespace Examen.helpers.database
{
    public class DatabaseHelper
    {
        private OleDbConnection database;


        public DatabaseHelper()
        {
            this.database = new OleDbConnection();


            this.initialzeDB();
            this.testConnection();
        }

        private void initialzeDB()
        {
            this.database.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + System.Web.HttpContext.Current.Server.MapPath(@"\App_data") + @"\JipEnJanneke.mdb";
            ;


        }

        private void testConnection()
        {
            try
            {
                this.database.Open();
                this.database.Close();
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }

        public List<List<object>> get(string database, string[] columnNames = null, object[] values = null)
        {
            try
            {
                this.database.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = this.database;
                string query = $"SELECT * FROM `{database}` ";
                int index = 0;

                if (columnNames != null && values != null)
                {
                    if (columnNames.Length != values.Length)
                    {
                        throw new Exception("columnNames.Length not same lenth as values.Length");
                    }
                    query += "WHERE ";
                    foreach (string culm in columnNames)
                    {
                        query += $"`{culm}` = {values[index]}";
                        index++;
                    }

                }
                command.CommandText = query;
                List<List<object>> dummy = new List<List<object>>();
                using (var myReader = command.ExecuteReader())
                {
                    while (myReader.Read())
                    {
                        List<object> dummyTwo = new List<object>();
                        for (int i = 0; i < myReader.FieldCount; i++)
                        {
                            dummyTwo.Add(myReader[i]);
                        }
                        dummy.Add(dummyTwo);
                    }
                }
                this.database.Close();
                return dummy;
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }

        }

        public void insert(string database, string[] columnNames, object[] values)
        {
            if (columnNames.Length != values.Length)
            {
                throw new Exception("columnNames.Length not same lenth as values.Length");
            }
            try
            {
                this.database.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = this.database;
                string query = $"INSERT INTO `{database}` ( ";
                int count = 1;
                foreach (string culm in columnNames)
                {
                    if (count == columnNames.Length)
                    {
                        query += $" `{culm}`) ";
                        break;
                    }
                    query += $"`{culm}`, ";
                    count++;
                }
                query += "VALUES ( ";
                count = 1;
                foreach (string val in values)
                {
                    if (count == values.Length)
                    {
                        query += $" '{val}') ";
                        break;
                    }
                    query += $"'{val}', ";
                    count++;
                }
                command.CommandText = query;

                command.ExecuteNonQuery();
                this.database.Close();
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
            
        }

        public void delete(string database, string where )
        {
            try
            {
                this.database.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = this.database;
                string query = $"DELETE * FROM `{database}` WHERE " + where;
              

                command.CommandText = query;

                command.ExecuteNonQuery();
                this.database.Close();
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }

        }

    }
}